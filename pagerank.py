# pagerank.py
"""Volume 1: The Page Rank Algorithm.
Spencer Giddens
Math 347
4/3/17
"""
from scipy.sparse import dok_matrix
import numpy as np
import scipy.linalg as la

A = np.array([[ 0, 0, 0, 0, 0, 0, 0, 1],
              [ 1, 0, 0, 0, 0, 0, 0, 0],
              [ 0, 0, 0, 0, 0, 0, 0, 0],
              [ 1, 0, 1, 0, 0, 0, 1, 0],
              [ 1, 0, 0, 0, 0, 1, 1, 0],
              [ 1, 0, 0, 0, 0, 0, 1, 0],
              [ 1, 0, 0, 0, 0, 0, 0, 0],
              [ 1, 0, 0, 0, 0, 0, 0, 0]])

# Problem 1
def to_matrix(filename, n):
    """Return the nxn adjacency matrix described by datafile.

    Parameters:
        datafile (str): The name of a .txt file describing a directed graph.
        Lines describing edges should have the form '<from node>\t<to node>\n'.
        The file may also include comments.
    n (int): The number of nodes in the graph described by datafile

    Returns:
        A SciPy sparse dok_matrix.
    """
    S = dok_matrix((n,n))
    with open ('./' + filename, 'r') as myfile:
        for line in myfile:
            try:
                index = map(int, line.strip().split())
                S[index[0], index[1]] = 1
            except:
                continue
    return S


# Problem 2
def calculateK(A,N):
    """Compute the matrix K as described in the lab.

    Parameters:
        A (ndarray): adjacency matrix of an array
        N (int): the datasize of the array

    Returns:
        K (ndarray)
    """
    #raise NotImplementedError("Problem 2 Incomplete")
    D = np.zeros(N)
    for i in xrange(N):
        if sum(A[i]) == 0:
            A[i] = np.ones(N)
        D[i] = sum(A[i])
    return A.T/D

# Problem 3
def iter_solve(adj, N=None, d=.85, tol=1E-5):
    """Return the page ranks of the network described by 'adj'.
    Iterate through the PageRank algorithm until the error is less than 'tol'.

    Parameters:
        adj (ndarray): The adjacency matrix of a directed graph.
        N (int): Restrict the computation to the first 'N' nodes of the graph.
            If N is None (default), use the entire matrix.
        d (float): The damping factor, a float between 0 and 1.
        tol (float): Stop iterating when the change in approximations to the
            solution is less than 'tol'.

    Returns:
        The approximation to the steady state.
    """
    #raise NotImplementedError("Problem 3 Incomplete")
    if N is None:
        N = len(adj)
    K = calculateK(adj, len(adj))[:N,:N]
    p0 = np.ones(N)/float(N)
    pnew = d*np.dot(K, p0) + ((1-d)/N)*np.ones(N)
    while la.norm(p0 - pnew) >= tol:
        p0 = pnew
        pnew = d*np.dot(K,p0) + ((1-d)/N)*np.ones(N)
    return pnew


# Problem 4
def eig_solve(adj, N=None, d=.85):
    """Return the page ranks of the network described by 'adj'. Use SciPy's
    eigenvalue solver to calculate the steady state of the PageRank algorithm

    Parameters:
        adj (ndarray): The adjacency matrix of a directed graph.
        N (int): Restrict the computation to the first 'N' nodes of the graph.
            If N is None (default), use the entire matrix.
        d (float): The damping factor, a float between 0 and 1.
        tol (float): Stop iterating when the change in approximations to the
            solution is less than 'tol'.

    Returns:
        The approximation to the steady state.
    """
    #raise NotImplementedError("Problem 4 Incomplete")
    if N is None:
        N = len(adj)
    K = calculateK(adj, len(adj))[:N,:N]
    E = np.ones_like(K)
    B = d*K + ((1-d)/N)*E
    eigs, vecs = la.eig(B)
    eigs = eigs.real
    ind = np.argsort(eigs)[-1]
    return (vecs[:,ind] / np.sum(vecs[:,ind])).real


# Problem 5
def team_rank(filename='ncaa2013.csv'):
    """Use iter_solve() to predict the rankings of the teams in the given
    dataset of games. The dataset should have two columns, representing
    winning and losing teams. Each row represents a game, with the winner on
    the left, loser on the right. Parse this data to create the adjacency
    matrix, and feed this into the solver to predict the team ranks.

    Parameters:
        filename (str): The name of the data file.
    Returns:
        ranks (list): The ranks of the teams from best to worst.
        teams (list): The names of the teams, also from best to worst.
    """
    #raise NotImplementedError("Problem 5 Incomplete")
    s = set()
    L = list()
    with open('./ncaa2013.csv', 'r') as ncaafile:
        ncaafile.readline()
        for i, line in enumerate(ncaafile):
            if i == 0:
                continue
            else:
                teams = line.strip().split(',')
                L.append(teams)
                s.add(teams[0])
                s.add(teams[1])
    N = len(s)
    A = np.zeros((N,N))
    d = dict()
    dinv = dict()
    for i, team in enumerate(s):
        d[team] = i
        dinv[i] = team
    for game in L:
        A[d[game[1]], d[game[0]]] = 1
    state = iter_solve(A, d=.7)
    sort = np.argsort(state)[::-1]
    f = lambda x: dinv[x]
    return state[sort], map(f, sort)
