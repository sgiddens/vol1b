# montecarlo_integration.py
"""Volume 1B: Monte Carlo Integration.
Spencer Giddens
Math 347
2/22/17
"""

import numpy as np
from scipy import stats
from matplotlib import pyplot as plt

# Problem 1
def prob1(N=100000):
    """Return an estimate of the volume of the unit sphere using Monte
    Carlo Integration.

    Input:
        N (int, optional) - The number of points to sample. Defaults
            to 10000.

    """
    #raise NotImplementedError("Problem 1 Incomplete")
    points = np.random.rand(3,N)
    points = points*2-1
    pointsDistances = np.linalg.norm(points,axis=0)
    numInCircle = np.count_nonzero(pointsDistances<1)
    circleArea = 8.*(float(numInCircle)/N)
    return circleArea


# Problem 2
def prob2(f, a, b, N=100000):
    """Use Monte-Carlo integration to approximate the integral of
    1-D function f on the interval [a,b].

    Inputs:
        f (function) - Function to integrate. Should take scalar input.
        a (float) - Left-hand side of interval.
        b (float) - Right-hand side of interval.
        N (int, optional) - The number of points to sample in
            the Monte-Carlo method. Defaults to 100000.

    Returns:
        estimate (float) - The result of the Monte-Carlo algorithm.

    Example:
        >>> f = lambda x: x**2
        >>> # Integral from 0 to 1. True value is 1/3.
        >>> prob2(f, 0, 1)
        0.3333057231764805
    """
    #raise NotImplementedError("Problem 2 Incomplete")
    points = np.random.rand(N)
    points = points*(b-a)+a
    value = 0.
    for i in xrange(N):
        value += f(points[i])
    est = ((b-a)/float(N))*value
    return est

# Problem 3
def prob3(f, mins, maxs, N=100000):
    """Use Monte-Carlo integration to approximate the integral of f
    on the box defined by mins and maxs.

    Inputs:
        f (function) - The function to integrate. This function should
            accept a 1-D NumPy array as input.
        mins (1-D np.ndarray) - Minimum bounds on integration.
        maxs (1-D np.ndarray) - Maximum bounds on integration.
        N (int, optional) - The number of points to sample in
            the Monte-Carlo method. Defaults to 10000.

    Returns:
        estimate (float) - The result of the Monte-Carlo algorithm.

    Example:
        >>> f = lambda x: np.hypot(x[0], x[1]) <= 1
        >>> # Integral over the square [-1,1] x [-1,1]. True value is pi.
        >>> mc_int(f, np.array([-1,-1]), np.array([1,1]))
        3.1290400000000007
    """
    #NotImplementedError("Problem 3 Incomplete")
    points = np.random.rand(len(maxs), N)
    for i in xrange(len(maxs)):
        points[i] = points[i]*(maxs[i]-mins[i]) + mins[i]
    volume = 1
    for i in xrange(len(maxs)):
        volume *= (maxs[i] - mins[i])
    tot = 0
    for i in xrange(N):
        tot += f(points[:,i])
    est = (volume/float(N))*tot
    return est



# Problem 4
def prob4():
    """Integrate the joint normal distribution.

    Return your Monte Carlo estimate, SciPy's answer, and (assuming SciPy is
    correct) the relative error of your Monte Carlo estimate.
    """
    #NotImplementedError("Problem 4 Incomplete")
    N = 4
    def f(x):
        return (1./(np.sqrt(2*np.pi)**N))*np.exp(-np.dot(x,x)/2.)
    mins = np.array([-1.5, 0., 0., 0.])
    maxs = np.array([0.75, 1., 0.5, 1.])
    est = prob3(f,mins,maxs)
    means = np.zeros(N)
    covs = np.eye(N)
    value, inform = stats.mvn.mvnun(mins, maxs, means, covs)
    return est, value, abs(est-value)



# Problem 5
def prob5(numEstimates=50):
    """Plot the error of Monte Carlo Integration."""
    #NotImplementedError("Problem 5 Incomplete")
    L = [50,100,500] + range(1000,50001,1000)
    y = []
    for N in L:
        avg_est = 0
        for _ in xrange(numEstimates):
            avg_est += prob1(N) - (4*np.pi/3)
        avg_est = avg_est / numEstimates
        y.append(abs(avg_est))
    plt.plot(L, y)
    L = np.array(L)
    f = lambda N: 1/np.sqrt(N)
    y = f(L)
    plt.plot(L,y)
    plt.show()
