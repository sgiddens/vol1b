# sympy_autograd.py
"""Volume 1B: Differentiation 2 (SymPy and Autograd).
Spencer Giddens
Math 347
1/17/2017
"""

import sympy as sy
import time
from autograd import grad
import autograd.numpy as np
from autograd import jacobian

def centered_difference_quotient(f, pts, h=1e-5):
    """Compute the centered difference quotient for function (f)
    given points (pts).

    Inputs:
        f (function): the function for which the derivative will be
            approximated.
        pts (array): array of values to calculate the derivative.

    Returns:
        An array of the centered difference quotient.
    """
    #raise NotImplementedError("Problem 1 Incomplete")
    a = []
    for pt in pts:
        value = ((f(pt + h)/2.) - (f(pt - h)/2.))/h
        a.append(value)
    return np.array(a)

# Problem 1
def myexp(n):
    """Compute e to the nth digit.

    Inputs:
        n (integer): n decimal places to calculate e.

    Returns:
        approximation (float): approximation of e.
    """
    #raise NotImplementedError("Problem 1 Incomplete")
    tot = sy.Rational(0,1)
    term = 1
    bound = sy.Rational(1,10)**(n+1)
    i = 0
    while bound <= term:
        term = sy.Rational(1 / sy.factorial(i))
        tot += term
        i+=1
    return sy.Float(tot, n)


# Problem 2
def prob2():
    """Solve y = e^x + x for x.

    Returns:
        the solution (list).
    """
    #raise NotImplementedError("Problem 2 Incomplete")
    x, y = sy.symbols('x,y')
    equation = sy.Eq(sy.exp(x)+x, y)
    return sy.solve(equation, x)


# Problem 3
def prob3():
    """Compute the integral of sin(x^2) from 0 to infinity.

    Returns:
        the integral value (float).
    """
    #raise NotImplementedError("Problem 3 Incomplete")
    x = sy.symbols('x')
    return sy.integrate(sy.sin(x**2), (x, 0, sy.oo))


# Problem 4
def prob4():
    """Calculate the derivative of e^sin(cos(x)) at x = 1.
    Time how long it takes to compute the derivative using SymPy as well as
    centered difference quotients.
    Calculate the error for each approximation.

    Print the time it takes to compute and the error for both SymPy and
    centered difference quotients.

    Returns:
        SymPy approximation (float)
    """
    #raise NotImplementedError("Problem 4 Incomplete")
    # Actual derivative
    df = lambda x: -np.sin(x)*np.cos(np.cos(x))*np.exp(np.sin(np.cos(x)))
    act = df(1)
    # Centered difference quotient way
    f = lambda x: np.exp(np.sin(np.cos(x)))
    pts = np.array([1])
    start = time.time()
    est1 = centered_difference_quotient(f, pts)[0]
    cdq_time = time.time() - start

    # SymPy way
    x = sy.symbols('x')
    expr = sy.exp(sy.sin(sy.cos(x)))
    start = time.time()
    expr = sy.diff(expr, x)
    est2 = expr.subs(x, 1)
    sympy_time = time.time() - start

    print "SymPy"
    print "Time:", sy.Float(sympy_time, 10)
    print "Error:", abs(est2 - act)
    print "Centered Difference Quotient"
    print "Time:", cdq_time
    print "Error:", abs(est1 - act)

    return est2



# Problem 5
def prob5():
    """Solve the differential equation when x = 1.

    Returns:
        Solution when x = 1.
    """
    #raise NotImplementedError("Problem 5 Incomplete")
    x = sy.symbols('x')
    f = sy.Function('f')
    eq = sy.Eq(f(x).diff(x, 6) + 3*f(x).diff(x,4) + 3*f(x).diff(x,2) + f(x), sy.exp(x)*x**10 + sy.sin(x)*x**11 + sy.exp(x)*sy.sin(x)*x**12 - sy.cos(2*x)*x**13 + sy.exp(x)*sy.cos(3*x)*x**14)
    return sy.dsolve(eq)


# Problem 6
def prob6():
    """Compute the derivative of ln(sqrt(sin(sqrt(x)))) at x = pi/4.
    Times how long it take to compute using SymPy, autograd, and centered
    difference quotients. Compute the error of each approximation.

    Print the time
    Print the error

    Returns:
        derviative (float): the derivative computed using autograd.
    """
    #raise NotImplementedError("Problem 6 Incomplete")
    f = lambda x: np.log(np.sqrt(np.sin(np.sqrt(x))))
    df = lambda x: 1. / (np.tan(np.sqrt(x))*4*np.sqrt(x))
    pts = np.array([np.pi/4])
    act = df(pts[0])

    # Centered Difference Quotient
    start = time.time()
    est1 = centered_difference_quotient(f, pts)[0]
    cdq_time = time.time() - start

    # Sympy
    x = sy.symbols('x')
    expr = sy.log(sy.sqrt(sy.sin(sy.sqrt(x))))
    start = time.time()
    expr = sy.diff(expr, x)
    est2 = expr.subs(x, pts[0])
    sympy_time = time.time() - start

    # Autograd
    start = time.time()
    grad_f = grad(f, argnum=0)
    est3 = grad_f(pts[0])
    autograd_time = time.time() - start

    # Print stuff
    print "SymPy"
    print "Time:", sy.Float(sympy_time, 10)
    print "Error:", abs(est2 - act)
    print "Autograd"
    print "Time:", autograd_time
    print "Error:", abs(est3 - act)
    print "Centered Difference Quotient"
    print "Time:", cdq_time
    print "Error:", abs(est1 - act)

    return est3




# Problem 7
def prob7():
    """Computes Jacobian for the function
        f(x,y)=[(e^x)sin(y) + y^3, 3y - cos(x)]
    Time how long it takes to compute the Jacobian using SymPy and autograd.

    Print the times.

    Returns:
        Jacobian (array): jacobian found using autograd at (x,y) = (1,1)
    """
    #raise NotImplementedError("Problem 7 Incomplete")
    f = lambda x: np.array([np.exp(x[0])*np.sin(x[1]) + x[1]**3, 3*x[1] - np.cos(x[0])])
    pt = np.array([1.,1.])

    # Autograd
    start = time.time()
    jacobian_f = jacobian(f)
    ret_value = jacobian_f(pt)
    autograd_time = time.time() - start

    # SymPy
    x, y = sy.symbols('x,y')
    start = time.time()
    F = sy.Matrix([sy.exp(x)*sy.sin(y) + y**3, 3*y - sy.cos(x)])
    F.jacobian([x,y])
    sympy_time = time.time() - start

    print "SymPy:", sympy_time
    print "Autograd", autograd_time

    return ret_value
