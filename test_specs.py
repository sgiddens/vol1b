# test_specs.py
"""Volume 1B: Testing.
Spencer Giddens
Math 347
1/10/2017
"""

import specs
import pytest
import math
import numpy as np

# Problem 1: Test the addition and fibonacci functions from specs.py
def test_addition():
    assert specs.addition(1, 2) == 3
    assert specs.addition(-1, -2) == -3

def test_smallest_factor():
    assert specs.smallest_factor(1) == 1
    assert specs.smallest_factor(4) == 2
    assert specs.smallest_factor(3) == 3

# Problem 2: Test the operator function from specs.py
def test_operator():
    assert specs.operator(1, 3, '+') == 4
    assert specs.operator(1, 3, '-') == -2
    assert specs.operator(1, 3, '*') == 3
    assert specs.operator(1, 2, '/') == .5
    assert specs.operator(5, 4, '/') == 1.25

    with pytest.raises(Exception) as excinfo:
        specs.operator(4, 0, '/')
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "You can't divide by zero!"

    with pytest.raises(Exception) as excinfo:
        specs.operator(1, 1, 2)
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Oper should be a string"

    with pytest.raises(Exception) as excinfo:
        specs.operator(1, 1, '++')
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == 'Oper should be one character'

    with pytest.raises(Exception) as excinfo:
        specs.operator(1, 1, 'h')
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Oper can only be: '+', '/', '-', or '*'"


# Problem 3: Finish testing the complex number class
@pytest.fixture
def set_up_complex_nums():
    number_1 = specs.ComplexNumber(1, 2)
    number_2 = specs.ComplexNumber(5, 5)
    number_3 = specs.ComplexNumber(2, 9)
    return number_1, number_2, number_3

def test_complex_init(set_up_complex_nums):
    number_1 = specs.ComplexNumber(1, 2)
    assert number_1.real == 1
    assert number_1.imag == 2
def test_complex_conjugate(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1.conjugate().real == 1 and number_1.conjugate().imag == -2
def test_complex_norm(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1.norm() == math.sqrt(5)
    assert number_2.norm() == math.sqrt(50)
    assert number_3.norm() == math.sqrt(85)
def test_complex_addition(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert (number_1 + number_2).real == 6 and (number_1 + number_2).imag == 7
def test_complex_subtraction(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert (number_1 - number_2).real == -4 and (number_1 - number_2).imag == -3
def test_complex_multiplication(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert (number_1 * number_2).real == -5 and (number_1 * number_2).imag == 15
def test_complex_division(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert (number_1 / number_2).real == .3 and (number_1 / number_2).imag == .1
    with pytest.raises(Exception) as excinfo:
        number_3 / specs.ComplexNumber(0, 0)
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Cannot divide by zero"
def test_complex_equals(set_up_complex_nums):
    number_1 = specs.ComplexNumber(1, 2)
    number_2 = specs.ComplexNumber(1, 2)
    assert (number_1 == number_2) == True
    assert (number_1 == specs.ComplexNumber(1, 1)) == False
def test_complex_string(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert str(number_1) == "1+2i"
    assert str(number_2) == "5+5i"
    assert str(number_3) == "2+9i"
    number_4 = specs.ComplexNumber(1, -2)
    assert str(number_4) == "1-2i"

# Problem 4: Write test cases for the Set game.
def test_playSet():
    assert specs.playSet('hands/hand1.txt') == 6

def test_veriFyle():
    with pytest.raises(Exception) as excinfo:
        specs.veriFyle('hands/hand0.txt') # File doesn't exist
    assert excinfo.typename == "ValueError"
    assert excinfo.value.args[0] == "Invalid file"

    assert specs.veriFyle('hands/hand1.txt') == 1 # Normal file

def test_parseArray():
    with pytest.raises(Exception) as excinfo:
        specs.parseArray('hands/hand2.txt') # File with 11 cards
    assert excinfo.typename == "ValueError"
    assert excinfo.value.args[0] == "There must be 12 cards in the hand"

    with pytest.raises(Exception) as excinfo:
        specs.parseArray('hands/hand3.txt') # File with number of 3 bits
    assert excinfo.typename == "ValueError"
    assert excinfo.value.args[0] == "Each number must be 4 bits"

    with pytest.raises(Exception) as excinfo:
        specs.parseArray('hands/hand4.txt') # File with weird characters and nums
    assert excinfo.typename == "ValueError"
    assert excinfo.value.args[0] == "Each number must consist only of 0's, 1's, and 2's"

    # Test verify duplicates here
    with pytest.raises(Exception) as excinfo:
        specs.parseArray('hands/hand5.txt') # File with duplicate cards
    assert excinfo.typename == "ValueError"
    assert excinfo.value.args[0] == "A hand cannot have duplicates"

def test_countMatches():
    c1 = np.array([0, 0, 0, 0])
    c2 = np.array([0, 1, 0, 0])
    c3 = np.array([1, 1, 1, 1])
    c4 = np.array([2, 0, 0, 2])
    c5 = np.array([1, 0, 2, 1])
    c6 = np.array([2, 2, 1, 2])
    c7 = np.array([2, 1, 2, 2])
    c8 = np.array([1, 0, 0, 1])
    c9 = np.array([0, 1, 1, 0])
    c10 = np.array([1, 2, 1, 1])
    c11 = np.array([2, 1, 2, 1])
    c12 = np.array([0, 0, 0, 1])
    L = [c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12]
    assert specs.countMatches(L) == 6
