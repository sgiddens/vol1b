# newtons_method.py
"""Volume 1B: Newton's Method.
Spencer Giddens
Math 347
1/11/2017
"""

import numpy as np
from matplotlib import pyplot as plt
import numpy.linalg as la
from numpy.linalg import inv

# Problem 1
def Newtons_method(f, x0, Df, iters=15, tol=1e-5, alpha=1):
    """Use Newton's method to approximate a zero of a function.

    Inputs:
        f (function): A function handle. Should represent a function
            from R to R.
        x0 (float): Initial guess.
        Df (function): A function handle. Should represent the derivative
             of f.
        iters (int): Maximum number of iterations before the function
            returns. Defaults to 15.
        tol (float): The function returns when the difference between
            successive approximations is less than tol.

    Returns:
        A tuple (x, converged, numiters) with
            x (float): the approximation for a zero of f.
            converged (bool): a Boolean telling whether Newton's method
                converged.
            numiters (int): the number of iterations computed.
    """
    #raise NotImplementedError("Problem 1 Incomplete")
    xold = x0
    i = 0
    flag = False
    while i < iters:
        i += 1
        xnew = xold - alpha*f(xold)/Df(xold)
        if abs(xnew - xold) < tol:
            flag = True
            break
        else:
            xold = xnew
    return xnew, flag, i


# Problem 2.1
def prob2_1():
    """Plot f(x) = sin(x)/x - x on [-4,4].
    Return the zero of this function to 7 digits of accuracy.
    """
    #raise NotImplementedError("Problem 2.1 Incomplete")
    def f(x):
        if x == 0:
            return 1
        else:
            return np.sin(x)/x - x

    def Df(x):
        if x == 0:
            return -1
        else:
            return ((x*np.cos(x))-np.sin(x))/(x**2) - 1

    x = np.linspace(-4, 4, 500)
    y = []
    for item in x:
        y.append(f(item))
    plt.plot(x, y)
    plt.show()

    x0 = 0

    return Newtons_method(f, x0, Df)[0]


# Problem 2.2
def prob2_2():
    """Return a string as to what happens and why during Newton's Method for
    the function f(x) = x^(1/3) where x_0 = .01.
    """
    #raise NotImplementedError("Problem 2.2 Incomplete")
    f = lambda x: np.sign(x)*np.power(np.abs(x), 1./3)
    x0 = .01
    Df = lambda x: 1. / (np.power(np.abs(x), 2./3)*3)

    print Newtons_method(f, x0, Df)

    return "The method does not converge because Df goes off to infinity as x0 approaches 0, making the derivative of poor use to estimate the zeros because the step size is too large."



# Problem 3
def prob3():
    """Given P1[(1+r)**N1-1] = P2[1-(1+r)**(-N2)], if N1 = 30, N2 = 20,
    P1 = 2000, and P2 = 8000, use Newton's method to determine r.
    Return r.
    """
    #raise NotImplementedError("Problem 3 Incomplete")
    N1, N2, P1, P2 = 30, 20, 2000, 8000
    f = lambda r: P1*((1+r)**N1 -1) - P2*(1 - (1+r)**-N2)
    x0 = .05
    Df = lambda r: P1*(N1*(1+r)**(N1-1)) - P2*(N2*(1+r)**(-N2 - 1))

    return Newtons_method(f, x0, Df)[0]

# Problem 4: Modify Newtons_method and implement this function.
def prob4():
    """Find an alpha < 1 so that running Newtons_method() on f(x) = x**(1/3)
    with x0 = .01 converges. Return the complete results of Newtons_method().
    """
    #raise NotImplementedError("Problem 4 Incomplete")
    f = lambda x: np.sign(x)*np.power(np.abs(x), 1./3)
    x0 = .01
    Df = lambda x: 1. / (np.power(np.abs(x), 2./3)*3)
    a=.3

    return Newtons_method(f, x0, Df, alpha=a)


# Problem 5: Implement Newtons_vector() to solve Bioremediation problem
def Newtons_vector(f, x0, Df, iters = 15, tol = 1e-5, alpha = 1):
    """Use Newton's method to approximate a zero of a vector valued function.

    Inputs:
        f (function): A function handle.
        x0 (list): Initial guess.
        Df (function): A function handle. Should represent the derivative
             of f.
        iters (int): Maximum number of iterations before the function
            returns. Defaults to 15.
        tol (float): The function returns when the difference between
            successive approximations is less than tol.
        alpha (float): Defaults to 1.  Allows backstepping.

    Returns:
        A tuple (x_values, y_values) where x_values and y_values are
        lists that contain the x and y value from each iteration of
        Newton's method
    """
    #raise NotImplementedError("Problem 5.1 Incomplete")
    x_values = [x0[0]]
    y_values =[x0[1]]
    xold = np.array(x0)
    i = 0
    flag = False
    while i < iters:
        i += 1
        xnew = xold - alpha*np.dot(inv(Df(xold)), f(xold))
        x_values.append(xnew[0])
        y_values.append(xnew[1])
        if la.norm(xnew - xold) < tol:
            flag = True
            break
        else:
            xold = xnew
    return x_values, y_values


def prob5():
    """Solve the system using Newton's method and Newton's method with
    backtracking
    """
    #raise NotImplementedError("Problem 5.2 Incomplete")
    gamma = 5
    delta = 1

    f = lambda x: np.array([gamma*x[0]*x[1] - x[0]*(1 + x[1]), -1*x[0]*x[1] + (delta - x[1])*(1 + x[1])])
    Df = lambda x: np.array([[gamma*x[1] - 1 - x[1], gamma*x[0] - x[0]],
                             [-1*x[1], -1*x[0] - 1 + delta - 2*x[1]]])
    alpha = 1
    x0 = np.array([-.2,.22])
    x, y =  Newtons_vector(f,x0,Df, alpha=alpha)

    alpha = .3
    xbar, ybar = Newtons_vector(f,x0,Df,alpha=alpha)

    plt.plot(x,y)
    plt.plot(xbar,ybar)
    plt.xlim(-8,10)
    plt.ylim(-3,4)

    x = np.linspace(-8,10,500)
    y = np.linspace(-3,4,500)
    X,Y = np.meshgrid(x,y)
    Z1, Z2 = f(np.array([X,Y]))

    plt.contour(X,Y, Z1, 0)
    plt.contour(X,Y, Z2, [-4, -2, 0, 2, 4])

    plt.show()


# Problem 6
def plot_basins(f, Df, roots, xmin, xmax, ymin, ymax, numpoints=1000, iters=15, colormap='brg'):
    """Plot the basins of attraction of f.

    INPUTS:
        f (function): Should represent a function from C to C.
        Df (function): Should be the derivative of f.
        roots (array): An array of the zeros of f.
        xmin, xmax, ymin, ymax (float,float,float,float): Scalars that define the domain
            for the plot.
        numpoints (int): A scalar that determines the resolution of the plot. Defaults to 100.
        iters (int): Number of times to iterate Newton's method. Defaults to 15.
        colormap (str): A colormap to use in the plot. Defaults to 'brg'.
    """
    #raise NotImplementedError("Problem 6 Incomplete")
    xreal = np.linspace(xmin, xmax, numpoints)
    ximag = np.linspace(ymin, ymax, numpoints)
    Xreal, Ximag = np.meshgrid(xreal, ximag)
    Xold = Xreal+1j*Ximag
    i = 0
    while i<iters:
        i +=1
        Xnew = Xold - f(Xold)/Df(Xold)
        Xold = Xnew
    A = np.zeros_like(Xnew)
    for i in xrange(A.shape[0]):
        for j in xrange(A.shape[1]):
            A[i,j] = np.abs(roots - Xnew[i,j]).argmin()
    plt.pcolormesh(Xreal,Ximag,A)
    plt.show()

# Problem 7
def prob7():
    """Run plot_basins() on the function f(x) = x^3 - 1 on the domain
    [-1.5,1.5]x[-1.5,1.5].
    """
    #raise NotImplementedError("Problem 7 Incomplete")
    f = lambda x: x**3 - 1
    Df = lambda x: 3*x**2
    roots = np.array([1, -.5 + 1j*np.sqrt(3)/2, -.5 - 1j*np.sqrt(3)/2])
    xmin, xmax, ymin, ymax = -1.5, 1.5, -1.5, 1.5
    plot_basins(f, Df, roots, xmin, xmax, ymin, ymax)
