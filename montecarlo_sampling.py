# montecarlo_sampling.py
"""Volume 1B: Monte Carlo 2 (Importance Sampling).
Spencer Giddens
Math 347
3/4/2017
"""
import numpy as np
from scipy import stats
from matplotlib import pyplot as plt
import time

# Problem 1
def prob1(n):
    """Approximate the probability that a random draw from the standard
    normal distribution will be greater than 3."""
    #raise NotImplementedError("Problem 1 Incomplete")
    a = np.random.randn(n)
    h = lambda x: x>=3
    return sum(map(h,a))/float(n)

# Problem 2
def prob2():
    """Answer the following question using importance sampling:
            A tech support hotline receives an average of 2 calls per
            minute. What is the probability that they will have to wait
            at least 10 minutes to receive 9 calls?
    Returns:
        IS (array) - an array of estimates using
            [5000, 10000, 15000, ..., 500000] as number of
            sample points."""
    #raise NotImplementedError("Problem 2 Incomplete")
    a = 9
    h = lambda x: x >= 10
    f = lambda x: stats.gamma.pdf(x, a, scale=.5)
    g = lambda x: stats.norm.pdf(x, loc=a+1)
    ret_arr = []
    for N in xrange(5000, 500001, 5000):
        X = np.random.normal(a+1, size=N)
        ret_arr.append(np.mean(h(X)*f(X)/g(X)))
    return np.array(ret_arr)

# Problem 3
def prob3():
    """Plot the errors of Monte Carlo Simulation vs Importance Sampling
    for the prob2()."""
    #raise NotImplementedError("Problem 3 Incomplete")
    imp_sample = prob2()
    h = lambda x: x > 10
    MC_estimates = []
    x_values = range(5000,505000,5000)
    for N in x_values:
        X = np.random.gamma(9,scale=.5,size=N)
        MC = 1./N*np.sum(h(X))
        MC_estimates.append(MC)
    MC_estimates = np.array(MC_estimates)

    act = 1 - stats.gamma(a=9, scale=.5).cdf(10)

    func = lambda x: abs(act - x)
    plt.plot(x_values, map(func, MC_estimates))
    plt.plot(x_values, map(func, imp_sample))
    plt.show()

# Problem 4
def prob4():
    """Approximate the probability that a random draw from the
    multivariate standard normal distribution will be less than -1 in
    the x-direction and greater than 1 in the y-direction."""
    #raise NotImplementedError("Problem 4 Incomplete")
    h = lambda x: x[0] < -1 and x[1] > 1
    f = lambda x: stats.multivariate_normal.pdf(x, np.zeros(2), np.eye(2))
    g = lambda x: stats.multivariate_normal.pdf(x, np.array([-1.5,1.5]), np.eye(2))
    N = 10**4
    X = np.random.multivariate_normal(np.array([-1.5,1.5]), np.eye(2), size=N)

    return 1./N * np.sum(np.apply_along_axis(h, 1, X) * np.apply_along_axis(f, 1, X) / np.apply_along_axis(g, 1, X))

def prob5():
    """ New problem"""
    def f():
        h = lambda y: y>10
        f = lambda y: stats.gamma(a=9,scale=.5).pdf(y)
        g = lambda y: stats.norm(loc=12,scale=2).pdf(y)
        samples = 500000
        Y = np.random.normal(12,2,samples)
        approx = 1./samples*np.sum(h(Y)*f(Y)/g(Y))
        return approx
    start = time.time()
    print f()
    samp_time = time.time() - start
    start = time.time()
    print 1 - stats.gamma(a=9,scale=.5).cdf(10)
    ind_time = time.time() - start
    return samp_time, ind_time
