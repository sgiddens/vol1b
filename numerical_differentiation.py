# numerical_differentiation.py
"""Volume 1B: Numerical Differentiation.
Spencer Giddens
Math 347
1/14/2017
"""

import numpy as np
import math
from scipy import linalg as la

# Problem 1
def centered_difference_quotient(f, pts, h=1e-5):
    """Compute the centered difference quotient for function (f)
    given points (pts).

    Inputs:
        f (function): the function for which the derivative will be
            approximated.
        pts (array): array of values to calculate the derivative.

    Returns:
        An array of the centered difference quotient.
    """
    #raise NotImplementedError("Problem 1 Incomplete")
    a = []
    for pt in pts:
        value = ((f(pt + h)/2.) - (f(pt - h)/2.))/h
        a.append(value)
    return np.array(a)

# Problem 2
def calculate_errors(f,df,pts,h = 1e-5):
    """Compute the errors using the centered difference quotient approximation.

    Inputs:
        f (function): the function for which the derivative will be
            approximated.
        df (function): the function of the derivative
        pts (array): array of values to calculate the derivative

    Returns:
        an array of the errors for the centered difference quotient
            approximation.
    """
    #raise NotImplementedError("Problem 2 Incomplete")
    calc_values = centered_difference_quotient(f, pts)
    act_values = []
    for pt in pts:
        act_values.append(df(pt))
    act_values = np.array(act_values)
    return calc_values - act_values

# Problem 3
def prob3():
    """Use the centered difference quotient to approximate the derivative of
    f(x)=(sin(x)+1)^x at x= pi/3, pi/4, and pi/6.
    Then compute the error of each approximation

    Returns:
        an array of the derivative approximations
        an array of the errors of the approximations
    """
    #raise NotImplementedError("Problem 3 Incomplete")
    pts = np.array([np.pi/3, np.pi/4, np.pi/6])
    f = lambda x: (np.sin(x)+1)**x
    df = lambda x: ((np.sin(x)+1)**x)*((np.log(np.sin(x)+1) + (x*np.cos(x))/(np.sin(x)+1)))
    return centered_difference_quotient(f,pts),calculate_errors(f,df,pts)

# Problem 4
def prob4():
    """Use centered difference quotients to calculate the speed v of the plane
    at t = 10 s

    Returns:
        (float) speed v of plane
    """
    #raise NotImplementedError("Problem 4 Incomplete")
    """def a(t):
        if t == 9:
            return np.radians(54.8)
        elif t == 10:
            return np.radians(54.06)
        elif t == 11:
            return np.radians(53.34)
    def b(t):
        if t == 9:
            return np.radians(65.59)
        elif t == 10:
            return np.radians(64.59)
        elif t == 11:
            return np.radians(63.62)

    def x(t):
        return 500*(np.tan(b(t)))/(np.tan(b(t))-np.tan(a(t)))
    def y(t):
        return 500*(np.tan(b(t))*np.tan(a(t)))/(np.tan(b(t))-np.tan(a(t)))

    pts = np.array([10])
    dx = centered_difference_quotient(x, pts, h=1)
    dy = centered_difference_quotient(y, pts, h=1)

    v = math.sqrt(dx[0]**2 + dy[0]**2)
    gamma = np.arcsin(dy/dx)
    return v"""

    a10 = np.radians(54.06)
    b10 = np.radians(64.59)
    da = np.radians((53.34-54.8)/2)
    db = np.radians((63.62-65.59)/2)
    sec2B = (1./np.cos(b10))**2
    sec2a = (1./np.cos(a10))**2
    dx = 500*((np.tan(b10)-np.tan(a10))*sec2B*db-np.tan(b10)*(sec2B*db-sec2a*da))/((np.tan(b10)-np.tan(a10))**2)
    dy = 500*((np.tan(b10)-np.tan(a10))*(np.tan(b10)*sec2a*da+sec2B*db*np.tan(a10))- (np.tan(b10)*np.tan(a10)*(sec2B*db-sec2a*da)))/((np.tan(b10)-np.tan(a10))**2)
    return np.sqrt(dy**2+dx**2)

# Problem 5
def jacobian(f, n, m, pt, h=1e-5):
    """Compute the approximate Jacobian matrix of f at pt using the centered
    difference quotient.

    Inputs:
        f (function): the multidimensional function for which the derivative
            will be approximated.
        n (int): dimension of the domain of f.
        m (int): dimension of the range of f.
        pt (array): an n-dimensional array representing a point in R^n.
        h (float): a float to use in the centered difference approximation.

    Returns:
        (ndarray) Jacobian matrix of f at pt using the centered difference
            quotient.
    """
    #raise NotImplementedError("Problem 5 Incomplete")
    J = np.zeros(m)
    for j in xrange(n):
        e_j = np.zeros_like(pt).astype(float)
        e_j[j] = h
        new = (f(pt + e_j)/2 - f(pt - e_j)/2)/h
        J = np.vstack((J, new))

    J = J[1:]
    return J.T

# Problem 6
def findError():
    """Compute the maximum error of jacobian() for the function
    f(x,y)=[(e^x)sin(y) + y^3, 3y - cos(x)] on the square [-1,1]x[-1,1].

    Returns:
        Maximum error of your jacobian function.
    """
    #raise NotImplementedError("Problem 6 Incomplete")
    f = lambda x: np.array([np.exp(x[0])*np.sin(x[1])+x[1]**3, 3*x[1] - np.cos(x[0])])
    df = lambda x: np.array([[np.exp(x[0])*np.sin(x[1]), np.exp(x[0])*np.cos(x[1]) + 3*x[1]**2],[np.sin(x[0]), 3]])
    m, n = 2, 2
    pts = np.linspace(-1,1,100)
    errorMatrix = np.zeros((100,100))
    for i in xrange(100):
        for j in xrange(100):
            pt = np.array([pts[i], pts[j]])
            actJ = df(pt)
            estJ = jacobian(f, n, m, pt)
            errorMatrix[i][j] = la.norm(actJ - estJ)
    return errorMatrix.max()
