# specs.py
"""Volume IB: Testing.
Spencer Giddens
1/10/2017
"""
import math
import numpy as np

# Problem 1 Write unit tests for addition().
# Be sure to install pytest-cov in order to see your code coverage change.
def addition(a,b):
    return a + b

def smallest_factor(n):
    """Finds the smallest prime factor of a number.
    Assume n is a positive integer.
    """
    if n == 1:
        return 1
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return i
    return n


# Problem 2 Write unit tests for operator().
def operator(a, b, oper):
    if type(oper) != str:
        raise ValueError("Oper should be a string")
    if len(oper) != 1:
        raise ValueError("Oper should be one character")
    if oper == "+":
        return a+b
    if oper == "/":
        if b == 0:
            raise ValueError("You can't divide by zero!")
        return a/float(b)
    if oper == "-":
        return a-b
    if oper == "*":
        return a*b
    else:
        raise ValueError("Oper can only be: '+', '/', '-', or '*'")

# Problem 3 Write unit test for this class.
class ComplexNumber(object):
    def __init__(self, real=0, imag=0):
        self.real = real
        self.imag = imag

    def conjugate(self):
        return ComplexNumber(self.real, -self.imag)

    def norm(self):
        return math.sqrt(self.real**2 + self.imag**2)

    def __add__(self, other):
        real = self.real + other.real
        imag = self.imag + other.imag
        return ComplexNumber(real, imag)

    def __sub__(self, other):
        real = self.real - other.real
        imag = self.imag - other.imag
        return ComplexNumber(real, imag)

    def __mul__(self, other):
        real = self.real*other.real - self.imag*other.imag
        imag = self.imag*other.real + other.imag*self.real
        return ComplexNumber(real, imag)

    def __div__(self, other):
        if other.real == 0 and other.imag == 0:
            raise ValueError("Cannot divide by zero")
        bottom = (other.conjugate()*other*1.).real
        top = self*other.conjugate()
        return ComplexNumber(top.real / bottom, top.imag / bottom)

    def __eq__(self, other):
        return self.imag == other.imag and self.real == other.real

    def __str__(self):
        return "{}{}{}i".format(self.real, '+' if self.imag >= 0 else '-',
                                                                abs(self.imag))

# Problem 5: Write code for the Set game here
def playSet(filename):
    veriFyle(filename)
    L = parseArray(filename)
    return countMatches(L)

def veriFyle(filename):
    try:
        with open(filename) as myFile:
            return 1
    except:
        raise ValueError("Invalid file")

def parseArray(filename):
    with open(filename) as myFile:
        L = myFile.read()
    L = L.split('\n')
    if len(L) != 12:
        raise ValueError("There must be 12 cards in the hand")
    ret_list = []
    for i in L:
        if len(i) != 4:
            raise ValueError("Each number must be 4 bits")
        for x in i:
            if x != '0' and x != '1' and x != '2':
                raise ValueError("Each number must consist only of 0's, 1's, and 2's")
        a = np.array([int(i[0]), int(i[1]), int(i[2]), int(i[3])])
        ret_list.append(a)
    # Test for duplicates
    for i, arr1 in enumerate(ret_list):
        for arr2 in ret_list[i+1:]:
            if all(arr1 == arr2):
                raise ValueError("A hand cannot have duplicates")
    return ret_list


def countMatches(L):
    matches = 0
    for i, arr1 in enumerate(L):
        for j, arr2 in enumerate(L[i+1:]):
            for arr3 in L[i + j + 2:]:
                arr = arr1 + arr2 + arr3
                if (arr[0] % 3) + (arr[1] % 3) + (arr[2] % 3) + (arr[3] % 3) == 0:
                    matches += 1
    return matches
